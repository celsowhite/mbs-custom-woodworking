<?php
/*
Template Name: Homepage
*/

get_header(); ?>

	<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<header id="home" class="page_header" style="background-image: url('<?php the_field('mbs_banner_image'); ?>')">
				<div class="callout_image">
					<img src="<?php the_field('mbs_custom_callout'); ?>" ?>
				</div>
			</header>

			<div class="page_content">

				<!-- About -->

				<section id="about">
					<div class="mbs_row">
						<div class="column_1_2">
							<?php the_content(); ?>
						</div>
						<div class="column_1_2">
							<div class="about_image">
								<?php the_post_thumbnail(); ?>
							</div>
						</div>
					</div>
				</section>

				<!-- Gallery -->

				<section id="gallery">
					<h1>Gallery</h1>
					<?php
					$project_args = array ('post_type' => 'mbs_projects', 'posts_per_page' => -1);
					$project_loop = new WP_Query($project_args);
					if ($project_loop -> have_posts()) : while ($project_loop -> have_posts()) : $project_loop -> the_post();
					?>	
						<a class="project_container" href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail(); ?>
							<div class="project_overlay">
								<p><?php the_title(); ?></p>
							</div>
						</a>
					<?php endwhile; endif; wp_reset_postdata(); ?>
				</section>

				<!-- Contact -->

				<section id="contact">
					<h1>Contact</h1>
					<div class="mbs_row">
						<div class="column_1_2">
							<?php the_field('contact_left_side'); ?>
						</div>
						<div class="column_1_2">
							<?php the_field('contact_right_side'); ?>
						</div>
					</div>
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2994.8897969781633!2d-73.42553004852138!3d41.35474900587816!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89e7ffcc2573658d%3A0x31e1a239d0800bda!2s25+Francis+J+Clarke+Cir%2C+Bethel%2C+CT+06801!5e0!3m2!1sen!2sus!4v1464637039369" frameborder="0" style="border:0" allowfullscreen></iframe>
				</section>
				
			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>
