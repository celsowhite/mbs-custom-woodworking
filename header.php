<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- Favicon -->

<link rel="icon" type="img/png" href="<?php the_field('mbs_favicon','option'); ?>" />

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', '_s' ); ?></a>

	<header class="site_header" role="banner">
		<nav class="main_navigation <?php if(!is_front_page()): ?>dark<?php endif; ?>" role="navigation">
			<a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>#home">
				<img class="white_logo" src="<?php the_field('mbs_logo','option'); ?>" />
				<img class="dark_logo" src="<?php the_field('mbs_alternate_logo','option'); ?>" />
			</a>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'container' => '' ) ); ?>
		</nav>
	</header>

	<div id="content" class="main_content">