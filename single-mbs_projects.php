<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package _s
 */

get_header(); ?>

	<main id="main post-<?php the_ID(); ?>" class="main_wrapper" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<div class="page_content">

				<!-- Header -->

				<header class="single_project_header">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</header>

				<!-- Project Gallery -->

				<section class="mbs_row mbs_gallery">
					<?php while(have_rows('gallery_images')): the_row(); ?>
						<div class="column_1_3">
							<?php
								$attachment_id = get_sub_field('gallery_image');
								$full_image_src = wp_get_attachment_image_src($attachment_id, 'full')[0];
								$thumbnail_image_src = wp_get_attachment_image_src($attachment_id, 'mbs_thumbnail')[0];
							?>
							<a href="<?php echo $full_image_src; ?>">
								<img src="<?php echo $thumbnail_image_src; ?>" />
							</a>
						</div>
					<?php endwhile; ?>
				</section>

				<!-- Additional Projects -->

				<section class="additional_projects">
					<h1>Other Projects</h1>
					<?php
					// Show all projects except the current project.
					$project_args = array ('post_type' => 'mbs_projects', 'posts_per_page' => -1, 'post__not_in' => array(get_the_ID()));
					$project_loop = new WP_Query($project_args);
					if ($project_loop -> have_posts()) : while ($project_loop -> have_posts()) : $project_loop -> the_post();
					?>	
						<a class="project_container" href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail(); ?>
							<div class="project_overlay">
								<p><?php the_title(); ?></p>
							</div>
						</a>
					<?php endwhile; endif; ?>
				</section>

			</div>

		<?php endwhile; ?>

	</main>

<?php get_footer(); ?>