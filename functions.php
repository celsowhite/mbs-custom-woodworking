<?php
/**
 * _s functions and definitions.
 *
 * @link https://codex.wordpress.org/Functions_File_Explained
 *
 * @package _s
 */

if ( ! function_exists( '_s_setup' ) ) :

/*=========================
Sets up theme defaults and registers support for various WordPress features.
 
Note that this function is hooked into the after_setup_theme hook, which
runs before the init hook. The init hook is too late for some features, such
as indicating support for post thumbnails.
========================*/

function _s_setup() {
	
	/*==========================================
	MAKE THEME AVAILABLE FOR TRANSLATION
	==========================================*/
	
	load_theme_textdomain( '_s', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.

	add_theme_support( 'automatic-feed-links' );

	/*==========================================
	LET WORDPRESS MANAGE THE DOCUMENT TITLE
	==========================================*/

	add_theme_support( 'title-tag' );

	/*==========================================
	ENABLE SUPPORT FOR POST THUMBNAILS ON POSTS AND PAGES
	==========================================*/

	add_theme_support( 'post-thumbnails' );

	/*==========================================
	NEW THUMBNAIL SIZE
	==========================================*/

	add_image_size( 'mbs_thumbnail', 500, 323, true);

	/*==========================================
	SETUP NAVIGATION MENUS
	==========================================*/

	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', '_s' ),
	) );

	/*==========================================
	Switch default core markup for search form, comment form, and comments
	to output valid HTML5.
	==========================================*/

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*==========================================
	ENABLE SUPPORT FOR POST FORMATS
	==========================================*/

	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

}
endif;

add_action( 'after_setup_theme', '_s_setup' );

 // Set the content width in pixels, based on the theme's design and stylesheet.

function _s_content_width() {
	$GLOBALS['content_width'] = apply_filters( '_s_content_width', 640 );
}
add_action( 'after_setup_theme', '_s_content_width', 0 );

/*==========================================
REMOVE WP EMOJI
==========================================*/

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

/*==========================================
REGISTER WIDGET AREA
==========================================*/

function _s_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', '_s' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', '_s_widgets_init' );

/*==========================================
ENQUEUE SCRIPTS AND STYLES
==========================================*/

function _s_scripts() {
	
	// Default theme style

	wp_enqueue_style( '_s-style', get_stylesheet_uri() );

	// Wordpress Default Jquery
	
	if (!is_admin()) {
		wp_deregister_script( 'jquery' );
		wp_deregister_script('jquery-migrate');
    	wp_register_script( 'jquery', includes_url( '/js/jquery/jquery.js' ), FALSE, NULL, TRUE);
		wp_register_script('jquery-migrate', includes_url('/js/jquery/jquery-migrate.min.js'), FALSE, NULL, TRUE);
		wp_enqueue_script('jquery');
		wp_enqueue_script('jquery-migrate');
	}

	// Google Fonts

	wp_enqueue_style( 'google_fonts', 'https://fonts.googleapis.com/css?family=Roboto:400,700|Old+Standard+TT:400,400italic' );

	// Magnific Popup

	wp_enqueue_style( 'magnific_styles', get_template_directory_uri() . '/css/magnific/magnific-popup.css' );

	wp_enqueue_script('magnific-js', get_template_directory_uri() . '/js/magnific/jquery.magnific-popup.min.js', '', '', true);

	// Compiled SCSS File

	wp_enqueue_style( 'custom_styles', get_template_directory_uri() . '/css/style.min.css' );

	// Font Awesome

	wp_enqueue_script('font-awesome', 'https://use.fontawesome.com/bfa5b116b0.js', '', '', true);

	// Smooth Scroll

	wp_enqueue_script('smooth-scroll', get_template_directory_uri() . '/js/smoothscroll/jquery.smooth-scroll.min.js', '', '', true);

	// Custom Scripts

	wp_enqueue_script('_s-scripts', get_template_directory_uri() . '/js/scripts.min.js', '', '', true);

	wp_enqueue_script( '_s-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}

add_action( 'wp_enqueue_scripts', '_s_scripts' );

/*==========================================
CUSTOM POST TYPES
==========================================*/

// Projects

function custom_post_type_projects() {
	$labels = array(
		'name'                => ('Projects'),
		'singular_name'       => ('Projects'),
		'menu_name'           => ('Projects'),
		'parent_item_colon'   => (''),
		'all_items'           => ('All Projects'),
		'view_item'           => ('View Project'),
		'add_new_item'        => ('Add New Project'),
		'add_new'             => ('Add New'),
		'edit_item'           => ('Edit Project'),
		'update_item'         => ('Update Projects'),
		'search_items'        => ('Search Projects'),
		'not_found'           => ('Not Found'),
		'not_found_in_trash'  => ('Not found in Trash'),
	);
	
	$args = array(
		'label'               => ('Projects'),
		'description'         => ('Projects'),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions' ),
		'hierarchical'        => false,
		'rewrite'             => array('slug' => 'project'),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'menu_icon'  		  => 'dashicons-format-gallery',
	);
	register_post_type( 'mbs_projects', $args );
}

add_action( 'init', 'custom_post_type_projects', 0 );

/*=============================================
ACF OPTIONS PAGE
=============================================*/

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page();
	
}

/*==========================================
LIMIT POST REVISIONS
==========================================*/

function limit_post_revisions( $num, $post ) {
    $num = 3;
    return $num;
}

add_filter( 'wp_revisions_to_keep', 'limit_post_revisions', 10, 2 );

/*=============================================
PAGE EXCERPTS
=============================================*/

function add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

add_action( 'init', 'add_excerpts_to_pages' );

/*=============================================
CUSTOM LOGIN SCREEN
=============================================*/

// Change the login logo URL

function my_loginURL() {
    return 'http://loginurl.com';
}
add_filter('login_headerurl', 'my_loginURL');

// Enque the login specific stylesheet for design customizations. CSS file is compiled through compass.

function my_logincustomCSSfile() {
    wp_enqueue_style('login-styles', get_template_directory_uri() . '/css/login.min.css');
}
add_action('login_enqueue_scripts', 'my_logincustomCSSfile');

/*=============================================
YOAST
=============================================*/

/*=== Adjust Metabox Priority ===*/

add_filter( 'wpseo_metabox_prio', function() { return 'low';});

/*==========================================
INCLUDES
==========================================*/

/*== Implement the Custom Header feature. ==*/

require get_template_directory() . '/inc/custom-header.php';

/*== Custom template tags for this theme. ==*/

require get_template_directory() . '/inc/template-tags.php';

/*== Custom functions that act independently of the theme templates. ==*/

require get_template_directory() . '/inc/extras.php';

/*== Customizer additions. ==*/

require get_template_directory() . '/inc/customizer.php';

/*== Load Jetpack compatibility file. ==*/

require get_template_directory() . '/inc/jetpack.php';
