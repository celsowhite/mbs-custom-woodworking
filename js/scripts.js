var $j = jQuery.noConflict();

$j(document).ready(function() {

	// Call these functions when the html has loaded.

	"use strict"; // Ensure javascript is in strict mode and catches errors.

	// Only change the header on scroll if on the homepage.

	if($j('body').hasClass('home')) {

		/*================================= 
		HEADER BAR
		=================================*/

		/*=== Adjust the header depending on scroll position. Also switch logos. ===*/
		
		$j(window).scroll(function(){
			var scroll = $j(window).scrollTop();
			if (scroll > 50) {
				$j('.main_navigation').addClass('dark');
			}
			else {
				$j('.main_navigation').removeClass('dark');
			}
		});

		/*=== Add Navigation Indicators ===*/

		$j(window).scroll(function(){

			// Set up positions of site sections

			var aboutPosition = $j('#about').offset().top - 150;
			var galleryPosition = $j('#gallery').offset().top - 150;
			var contactPosition = $j('#contact').offset().top - 150;
			var scroll = $j(this).scrollTop();

			if(aboutPosition <= scroll && scroll <= galleryPosition) {
				$j('#primary-menu li a').removeClass('active');
				$j('#menu-item-23 a').addClass('active');
			}
			else if (galleryPosition <= scroll && scroll <= contactPosition) {
				$j('#primary-menu li a').removeClass('active');
				$j('#menu-item-24 a').addClass('active');
			}
			else if (contactPosition <= scroll) {
				$j('#primary-menu li a').removeClass('active');
				$j('#menu-item-25 a').addClass('active');
			}
			else {
				$j('#primary-menu li a').removeClass('active');
			}

		});

	};

	/*================================= 
	MAGNIFIC IMAGE GALLERY
	=================================*/

	$j('.mbs_gallery').magnificPopup({
	  delegate: 'a',
	  type: 'image',
	  gallery:{
	    enabled: true
	  }
	});

	/*================================= 
	SMOOTH SCROLL
	=================================*/

	$j('a').smoothScroll({
		offset: -120
	});

});